-- Find all artists that has letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that has a length of less than 230
SELECT * FROM songs where length < 230;

-- Join the 'albums' and 'songs' tables.(Only show the album name, song name, and song length.)
SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;
